import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AppctrlsidebarComponent } from './appctrlsidebar.component';

describe('AppctrlsidebarComponent', () => {
  let component: AppctrlsidebarComponent;
  let fixture: ComponentFixture<AppctrlsidebarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AppctrlsidebarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AppctrlsidebarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
